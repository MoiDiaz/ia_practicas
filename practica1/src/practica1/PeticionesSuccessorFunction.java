package practica1;

import java.util.ArrayList;
import java.util.List;

import aima.search.framework.Successor;
import aima.search.framework.SuccessorFunction;

public class PeticionesSuccessorFunction implements SuccessorFunction{
	public List getSuccessors(Object aState) {
	    ArrayList retVal= new ArrayList();
	    Representacion representacion=(Representacion) aState;
	    int peti = representacion.getPeticion();
	    
	    //System.out.println("\nSucesores AQUI ENTRA  -->");
	   // System.out.println(representacion.getPeticiones().size());
	    
	    for(int i = 0; i<representacion.getCamiones().size(); ++i){
	    	Representacion newRepresentacion = new Representacion(representacion.getCamiones(), representacion.getPeticiones(), representacion.getCentrosDist(), representacion.getGasolineras(), representacion.getBeneficio());
	    	String S="";
	    	//System.out.println("\n"+newRepresentacion.getGasolineras().get(peti%100).getCoordX()+ " "+ newRepresentacion.getGasolineras().get(peti%100).getCoordY()+" " + newRepresentacion.getCamiones().get(i).getXcoord() +" "+newRepresentacion.getCamiones().get(i).getYcoord());
			
	    		
	    	
	    	if(representacion.getCamiones().get(i).canMoveTo(newRepresentacion.getGasolineras().get(peti%100).getCoordX(), newRepresentacion.getGasolineras().get(peti%100).getCoordY())){
	    		newRepresentacion.asignarPeticion(peti, i);
	    		S += "Se le asigna peticion al camion cisterna: "+i+" de la gasolinera "+(peti%100);
	    		S += "----> Beneficio actual: "+-(newRepresentacion.getBeneficio());
	    		//System.out.println("\n"+representacion.getCamiones().get(i).empty());
	    		if(newRepresentacion.getCamiones().get(i).empty()){
					newRepresentacion.repostarCamion(i);
					S += "\nEl camion cisterna "+i+" vuelve para repostar  "; 
		    	}
	    		//if( i<9)System.out.println("\n"+newRepresentacion.getCamiones().get(i+1).getPeticiones().size()+ " " +newRepresentacion.getCamiones().get(i).getPeticiones().get(0));
	    		//System.out.println("\nSe le asigna peticion al camion cisterna "+i+" de la gasolinera "+(peti%100));
	    	}
	    	retVal.add(new Successor(S, newRepresentacion));
	    }
	    
	   // System.out.println("\n "+retVal.size()+ " --> sucesores");
	    return retVal;
	}

}

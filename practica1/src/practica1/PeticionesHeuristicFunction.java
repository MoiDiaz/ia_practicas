package practica1;

import aima.search.framework.HeuristicFunction;

public class PeticionesHeuristicFunction implements HeuristicFunction {
	
	public double getHeuristicValue(Object state) {
		Representacion respresentacion = (Representacion) state;
		 //System.out.println("\n Heuristico AQUI ENTRA  -->");
		 //System.out.println("\n "+respresentacion.getBeneficio());
		 
		return respresentacion.getBeneficio();
		
	}

}

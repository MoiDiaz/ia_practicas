package practica1;

import IA.Gasolina.*;
import aima.*;
import java.util.ArrayList;
import java.util.Collections;
import java.math.*;

public class Representacion {
	
	
	private ArrayList<Camion> camiones;
	private CentrosDistribucion centrosDist;
	private Gasolineras gasolineras;
	private ArrayList<Integer> peticionesGas;
	private double beneficios;
	
	public Representacion(int nCen, int nGas, int mult, int seed){
//creamos la lista de camiones;
		centrosDist = new CentrosDistribucion(nCen, mult, seed);
		camiones = new ArrayList<Camion>();
		for(int i = 0; i<centrosDist.size(); ++i){
			Camion cam = new Camion(centrosDist.get(i).getCoordX(),centrosDist.get(i).getCoordY());
			camiones.add(cam);
		}
		gasolineras = new Gasolineras(nGas, seed);
		
//guardamos las peticiones de todas las gasolineras.	
		peticionesGas = new ArrayList<Integer>();
		for(int i= 0; i<gasolineras.size(); ++i ){
			// System.out.println("\n"+gasolineras.get(i).getCoordX()+ " "+ camiones.get(i).getXcoord()+" " + gasolineras.get(i).getCoordY() +" "+camiones.get(i).getYcoord());
			for(int j = 0; j<gasolineras.get(i).getPeticiones().size(); ++j){
				peticionesGas.add(gasolineras.get(i).getPeticiones().get(j)*100+i);
			}
		}
		/*Collections.sort(peticionesGas);
		Collections.reverse(peticionesGas);*/
		beneficios = 0;
	}
	
	public Representacion(ArrayList<Camion> cL, ArrayList<Integer> p, CentrosDistribucion cD, Gasolineras g, double b){
		
		camiones = new ArrayList<Camion>();
		for(int i = 0; i < cL.size(); ++i){
			Camion c = new Camion (cL.get(i));
			camiones.add(c);
		}
		peticionesGas = new ArrayList<Integer>();
		for(int i = 0; i < p.size(); ++i){
			peticionesGas.add(p.get(i));
		}
		gasolineras = g;
		centrosDist = cD;
		beneficios = b;
	}
	
	public Representacion() {
		// TODO Auto-generated constructor stub
	}

	public ArrayList<Camion> getCamiones(){return camiones;}
	public CentrosDistribucion getCentrosDist(){return centrosDist;}
	public Gasolineras getGasolineras(){return gasolineras;}
	public ArrayList<Integer> getPeticiones(){return peticionesGas;}
	public int getPeticion(){return peticionesGas.get(0);}
	
	
	
	public boolean isGoal(){
		for(int i = 0; i<camiones.size(); ++i){
			if(camiones.get(i).remainingTrips()>0 || camiones.get(i).remainingTrips() == 5){
				for(int j = 0; j<gasolineras.size(); ++j){
					if(camiones.get(i).canMoveTo(gasolineras.get(j).getCoordX(), gasolineras.get(j).getCoordY()) ) return false ;
				}
			}
		}
		return true;
	}
	
	public void asignarPeticion(int peticion, int idCamion){
		int idGas = peticion%100;
		camiones.get(idCamion).addPeticion(peticion);
		camiones.get(idCamion).useDeposit();
		double dist = camiones.get(idCamion).moveCamion(gasolineras.get(idGas).getCoordX(), gasolineras.get(idGas).getCoordY());
		double dias = peticion/100;
		
		//System.out.println("\n"+dist+"<--------dist");
		//System.out.println("\n"+peticion+ " "+idCamion +" <----- peticion");
		
		if(dias == 0) beneficios -= 10.0*102;
		else beneficios -= 10.0*(100.0-Math.pow(2.0,dias));
		//System.out.println("\n"+beneficios+" <----- beneficios");
		//beneficios -= 2*( Math.abs(gasolineras.get(idGas).getCoordX() - camiones.get(idCamion).getXcoord()) + Math.abs(gasolineras.get(idGas).getCoordY() - camiones.get(idCamion).getYcoord()) );
		beneficios += 2*dist;
		 //System.out.println("\n"+beneficios+" <----- beneficios");
		// System.out.println("\n"+gasolineras.get(idGas).getCoordX()+ " "+ camiones.get(idCamion).getXcoord()+" " + gasolineras.get(idGas).getCoordY() +" "+camiones.get(idCamion).getYcoord());
		peticionesGas.remove(0);
	}
	
	public double getBeneficio(){
		return beneficios;
	}
	
	public void repostarCamion(int idCamion){
		double dist = camiones.get(idCamion).moveCamion(camiones.get(idCamion).getXCoordC(), camiones.get(idCamion).getYCoordC());
		//beneficios -= 2*( Math.abs(camiones.get(idCamion).getXCoordC() - camiones.get(idCamion).getXcoord()) + Math.abs(camiones.get(idCamion).getYCoordC() - camiones.get(idCamion).getYcoord()) );
		beneficios += 2*dist;
		camiones.get(idCamion).fillDeposit();
	}
	
}

package practica1;

import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import IA.TSP2.ProbTSPBoard;
import IA.TSP2.ProbTSPGoalTest;
import IA.TSP2.ProbTSPHeuristicFunction;
import IA.TSP2.ProbTSPSuccessorFunction;
import IA.probTSP.ProbTSPSuccessorFunctionSA;
import aima.search.framework.GraphSearch;
import aima.search.framework.Problem;
import aima.search.framework.Search;
import aima.search.framework.SearchAgent;
import aima.search.framework.TreeSearch;
import aima.search.informed.AStarSearch;
import aima.search.informed.HillClimbingSearch;
import aima.search.uninformed.BreadthFirstSearch;
import aima.search.uninformed.DepthLimitedSearch;
import aima.search.uninformed.IterativeDeepeningSearch;
import aima.search.informed.IterativeDeepeningAStarSearch;
import aima.search.informed.SimulatedAnnealingSearch;

public class CamionesDemo {

	 public static void main(String[] args){
		 Random ran = new Random(System.currentTimeMillis());
	        Representacion repre = new Representacion(10,100,1,(ran.nextInt()));
	        System.out.println("\n numero de peticiones "+repre.getPeticiones().size());
	        
	        PeticionesHillClimbingSearch(repre);
	        PeticionesSimulatedAnnealingSearch(repre);
	 }
	 private static void PeticionesHillClimbingSearch(Representacion r) {
	        System.out.println("\nAsignacionDePeticiones HillClimbing  -->");
	        try {
	            Problem problem =  new Problem(r,new PeticionesSuccessorFunction(), new CamionesGoalTest(),new PeticionesHeuristicFunction());
	            Search search =  new HillClimbingSearch();
	            SearchAgent agent = new SearchAgent(problem,search);
	            
	            System.out.println();
	            printActions(agent.getActions());
	            printInstrumentation(agent.getInstrumentation());
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        System.out.println("\n Ciao pescao HillClimbing  -->");
	    }
	 
	 private static void PeticionesSimulatedAnnealingSearch(Representacion r) {
	        System.out.println("\nAsignacionDePeticiones Simulated Annealing  -->");
	        try {
	            Problem problem =  new Problem(r,new PeticionesSuccessorFunction(), new CamionesGoalTest(),new PeticionesHeuristicFunction());
	            SimulatedAnnealingSearch search =  new SimulatedAnnealingSearch(2000,100,5,0.001);
	            //search.traceOn();
	            SearchAgent agent = new SearchAgent(problem,search);
	            
	            System.out.println();
	            printActions(agent.getActions());
	            printInstrumentation(agent.getInstrumentation());
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	 }
	 
	 private static void printInstrumentation(Properties properties) {
	        Iterator keys = properties.keySet().iterator();
	        while (keys.hasNext()) {
	            String key = (String) keys.next();
	            String property = properties.getProperty(key);
	            System.out.println(key + " : " + property);
	        }
	        
	    }
	    
	    private static void printActions(List actions) {
	        for (int i = 0; i < actions.size(); i++) {
	            String action = (String) actions.get(i);
	            System.out.println(action);
	        }
	    }
}

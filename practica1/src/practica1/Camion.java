package practica1;
import java.util.ArrayList;


public class Camion {
	
	private int xCoord;
	private int yCoord;
	private static int xCoordC;
	private static int yCoordC;
	private int kmMax;
	private int trips;
	private boolean deposit1;
	private boolean deposit2;
	private ArrayList<Integer> peticiones;
	
	
	public Camion(int x, int y){
		xCoord = x;
		yCoord = y;
		xCoordC = x;
		yCoordC = y;
		kmMax = 640;
		trips = 5;
		deposit1 = true;
		deposit2 = true;
		peticiones = new ArrayList<Integer>();
	}
	
	public Camion(Camion c){
		xCoord = c.xCoord;
		yCoord = c.yCoord;
		xCoordC = c.xCoordC;
		yCoordC = c.yCoordC;
		kmMax = c.kmMax;
		trips = c.trips;
		deposit1 = c.deposit1;
		deposit2 = c.deposit2;
		peticiones = new ArrayList<Integer>();
		for(int i = 0; i<c.peticiones.size();++i ){
			peticiones.add(c.peticiones.get(i));
		}
	}
	
	public int getXcoord(){
		return xCoord;
	}
	
	public int getYcoord(){
		return yCoord;
	}
	
	public int getXCoordC() {
		return xCoordC;
	}
	
	public int getYCoordC() {
		return yCoordC;
	}
	
	public boolean empty(){
		return (deposit1 == false && deposit2 == false);
	}
	
	public void fillDeposit(){
		deposit1 = true;
		deposit2 = true;
		--trips;
	}
	
	public void useDeposit(){
		if(deposit1 == true) deposit1 = false;
		else deposit2 = false;
	}
	
	public boolean canMoveTo(int x, int y){	
		int vuelta = Math.abs(xCoordC - x) + Math.abs(yCoordC - y);
		int ida = Math.abs(x - xCoord) + Math.abs(y - yCoord);
		
		return trips>0 && kmMax-ida-vuelta >= 0;
	}
	
	public int remainingTrips() {
		return trips;
	}
	
	public double moveCamion(int x, int y){
		double dist =  Math.abs(xCoord - x) + Math.abs(yCoord - y);
		kmMax -= dist;
		xCoord = x;
		yCoord = y;
		return dist;
	}
	public ArrayList<Integer> getPeticiones(){
		return peticiones;
	}
	
	public boolean addPeticion(int peticion){
		if(peticiones.size()>=10) return false;
		else{
			peticiones.add(peticion);
			return true;
		}
		
	}

}

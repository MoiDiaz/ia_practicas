package practica1;

import aima.search.framework.GoalTest;

public class CamionesGoalTest implements GoalTest{

	@Override
	public boolean isGoalState(Object state) {
		System.out.println("\nGoalTest AQUI ENTRA ------>");
		Representacion rep = (Representacion) state;
		
		return rep.isGoal();
	}

}
